#!/usr/bin/env python3

from poli_login import get_polimi_token
from stream import *
import argparse
import re
import getpass

def get_id_from_url(url):
	regex = r'[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}'
	result = re.search(regex, url)
	if result != None:
		video_id = result[0]
		return video_id
	return None

def read_file(path):
	ids = []
	with open(path, "r") as f:
		lines = f.readlines()
	for line in lines:
		video_id = get_id_from_url(line)
		if video_id != None:
			ids.append(video_id)
	return ids


def do_magic(username, password, URL ,dest_path, file=None):

	try:
		token = get_polimi_token(username, password)
	except:
		return False

	if file != None:
		video_ids = read_file(file)
	else:
		video_ids = [get_id_from_url(URL)]

	ms_stream = MsStream(token)

	to_download = []

	for video_id in video_ids:
		to_download.append(ms_stream.get_video_by_id(video_id))

	for video in to_download:
		video.download()


if __name__ == "__main__":

	parser = argparse.ArgumentParser()

	parser.add_argument('-u', action='store', dest='url',
	                    help='Url of the lecture')

	parser.add_argument('-i', action='store', dest='file',
	                    help='File with urls')

	results = parser.parse_args()

	if results.file != None:
		video_ids = read_file(results.file)
	else:
		video_ids = [get_id_from_url(results.url)]

	username = input("Codice persona:")

	password = getpass.getpass(prompt='Polimi passwd: ') 

	token = get_polimi_token(username, password)

	ms_stream = MsStream(token)

	to_download = []

	for video_id in video_ids:
		to_download.append(ms_stream.get_video_by_id(video_id))

	for video in to_download:
		video.download()



