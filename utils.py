import m3u8
from base64 import b64encode

def get_playlist(url, max_bandwidth=999999999):
	m3u8_obj =  m3u8.load(url)
	curr_band = 0
	for playlist in m3u8_obj.playlists:
		if playlist.stream_info.resolution == None:
			audio = m3u8.load(playlist.absolute_uri)
			continue
		# If max_bandwidth is too low it will pick the audio and crash somewhere. RIP
		if playlist.stream_info.bandwidth > curr_band and curr_band < max_bandwidth:
			video = m3u8.load(playlist.absolute_uri)
			curr_band = playlist.stream_info.bandwidth
	return (video, audio)


def get_key(session, playlist):
	uri = playlist.keys[0].absolute_uri
	return b64encode(session.get(uri).content)


def get_subs(session, stream):
	resp = session.get("https://euwe-1.api.microsoftstream.com/api/videos/{}/texttracks?api-version=1.3-private".format(stream["id"])).json()["value"]
	if len(resp) != 0:
		subs = session.get(resp[0]["url"]).content.decode("utf-8")
		return subs
	return "RIP"
