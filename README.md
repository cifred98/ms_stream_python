# Ms Stream Downloader


## Requirements 

- python-m3u8
- aria2
- ffmpeg
- enough storage :))))


## How to use

### Download a single video

```bash
./main.py -u https://web.microsoftstream.com/video/xxxx-xxxx-xxxx-xxxxxxxx

```


### Download from file
- The file must have a link for each line
```bash
./main.py -i file.txt
```